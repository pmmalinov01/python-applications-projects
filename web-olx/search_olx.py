#!/usr/bin/env python3

import requests, pandas
from pprint import pprint
from bs4 import BeautifulSoup

results = []
laptops = None
laptop_details = {}
page = 0


url = "https://www.olx.bg/"
headers = {
    "User-agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0"
}
query = "elektronika/kompyutri/laptopi/?search"

olx_home_page = requests.get(url + query, headers=headers,)


soup = BeautifulSoup(olx_home_page.content, "lxml")


def find_links(soup_page):

    promo_laptops = soup_page.find_all(
        "table", class_="fixed offers breakword offers--top redesigned"
    )
    for each_laptop in promo_laptops:
        laptop_links = [
            x["href"]
            for x in each_laptop.find_all(
                "a", class_="marginright5 link linkWithHash detailsLinkPromoted"
            )
        ]
        laptop_price = [
            laptop.text for laptop in each_laptop.find_all("p", class_="price")
        ]
        laptop_details = dict(zip(laptop_links, laptop_price))

    df = pandas.DataFrame.from_dict(laptop_details, orient="index")
    print(df)
    df.to_csv(
        r"C:\Users\pavm\Documents\Python Scripts\python-applications-projects\web-olx\Prices.csv",
        mode="a",
        header=False,
    )

    return laptop_links, laptop_price, laptop_details


def last_page(page):
    # last_page_number = soup.find_all("a", class_="block br3 brc8 large tdnone lheight24", data-cy_="page-link-last")
    all_pages = soup.find_all("a", class_="block br3 brc8 large tdnone lheight24")
    page_number = [page_number.get_text() for page_number in all_pages]
    print(page_number[-1])

    while page < int(page_number[-1]):

        page += 1
        print(page)
        # https://www.olx.bg/elektronika/kompyutri/laptopi/?search&page=185
        olx_next_page = requests.get(url + query + f"&page={page}", headers=headers,)
        print(olx_next_page.url)

        soup_page = BeautifulSoup(olx_next_page.content, "lxml")
        find_links(soup_page)
    return page_number[-1]


last_page(page)
