#!/usr/bin/env python3

from tkinter import Tk, Label, Frame, Entry, Button, StringVar, Text, END


window = Tk()


def km_to_miles():
    print(e1_value.get())
    miles = float(e1_value.get())*1.6
    t1.insert(END, miles)


button1 = Button(window, text="Execute", command=km_to_miles)
button1.grid(row=2, column=4)


e1_value = StringVar()
entry1 = Entry(window, textvariable=e1_value)
entry1.grid(row=0, column=1)


t1 = Text(window, height=1, width=30)
t1.grid(row=0, column=2)


window.mainloop()
