#!/usr/bin/env python3

import pytz
import time
from datetime import datetime as dt

host = "hosts"
redirect = "127.0.0.1"

website_to_block = ["www.facebook.com", "facebook.com"]
print(dt(dt.now().year, 1,2,4))

while True:
    if dt(dt.now().year, dt.now().month, dt.now().day, 17) < dt.now() < dt(dt.now().year, dt.now().month, dt.now().day, 18):
        print("Working Hourse...")
        with open(host, 'r+') as file:
            content = file.read()
            for website in website_to_block:
                if website in content:
                    pass
                else:
                    file.write(f"{redirect} {website}\n")
    else:
        with open(host, "r+") as file:
            content = file.readline()
            for line in content:
                if not any(website in line for website in website_to_block):
                    file.write(line)
            
        print("Have fun to browse...")

    time.sleep(3)
