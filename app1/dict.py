#!/usr/bin/env python3


import json
from difflib import get_close_matches

data = json.load(open("data.json"))


def search_key(value):
    value = value.lower()
    if value in data:
        return data[value]
    elif len(get_close_matches(value, data.keys())) > 0:
        confirm = input(f"Did you wanted {get_close_matches(value, data.keys())[0]}?")
        if confirm == "Y":
            value = get_close_matches(value, data.keys())[0]
            return data[value]
        else:
            return "Byee!"
    else:
        return "No such word in data.json"


word = input("Search for word:\n")


output = search_key(word)

if type(output) == list:
    for item in output:
        print(item)
else:
    print(output)
