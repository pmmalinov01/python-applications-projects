import cv2
import pandas
from datetime import datetime as dt

video = cv2.VideoCapture(0)
first_frame = None
time_obj_cap = []
status = 0
status_list=[None, None]

df  = pandas.DataFrame(columns=["start", "end"])
while True:

    check, frame = video.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGRA2GRAY)
    gray = cv2.GaussianBlur(gray,(21, 21), 0)

    # first frame needs to be without human to make it the one it comperes to
    if first_frame is None:
        first_frame=gray
        continue

    delta_frame = cv2.absdiff(first_frame, gray)
    thresh_frame = cv2.threshold(delta_frame, 30, 255, cv2.THRESH_BINARY)[1]
    thresh_frame = cv2.dilate(thresh_frame, None, iterations=2)

    (cnts,_) = cv2.findContours(thresh_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for contour in cnts:
        if cv2.contourArea(contour) < 10000:
            continue
        status = 1

        (x, y ,w ,h) = cv2.boundingRect(contour)
        cv2.rectangle(frame, (x,y), (x+w, y+h), (1,255,1), 3)

    status_list.append(status)


    if status_list[-1] == 1 and status_list[-2] == 0:
        time_obj_cap.append(dt.now().time())

    if status_list[-1] == 0 and status_list[-2] == 1:
        time_obj_cap.append(dt.now().time())

    # cv2.imshow("Threshold frame", thresh_frame)
    # cv2.imshow("Delta_Frame", delta_frame)    
    # cv2.imshow("Cap" , gray)
    cv2.imshow("rectangler", frame)
    #print(time_obj_cap)
    key = cv2.waitKey(1)
    if key==ord('q'):
        if status == 1:
            time_obj_cap.append(dt.now().time())
        break


for i in range(0,len(time_obj_cap), 2):
    df = df.append({"Start":time_obj_cap[i], "End": time_obj_cap[i+1]}, ignore_index=True)

df.to_csv("Times.csv")

print(time_obj_cap)
video.release()
cv2.destroyAllWindows